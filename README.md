# certificate authority on a raspberry pi

This is a repository with code to setup a CA on the Raspberry Pi.
[This](https://jamielinux.com/docs/openssl-certificate-authority/) tutorial was used. Thanks to Jamie Nguyen.