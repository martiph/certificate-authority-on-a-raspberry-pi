# Setup a CA on a Raspberry Pi

## Introduction

As part of the after-work to INSich2, a CA is needed to setup. To do the setup, OpenSSL is used.  
[This](https://jamielinux.com/docs/openssl-certificate-authority/) tutorial is used to setup the CA.  

