#!/bin/bash

# prepare the directory
cd /
mkdir /root/ca
cd /root/ca
mkdir certs crl newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial

# prepare the configuration file
wget https://gitlab.com/martiph/certificate-authority-on-a-raspberry-pi/raw/master/root.openssl.cnf 
mv ./root.openssl.cnf /root/ca/openssl.cnf

# now you need to edit the configuration file

# after you have done the modifications
cd /root/ca
openssl genrsa -aes256 -out private/ca.key.pem 4096

# now add a secure password

chmod 400 private/ca.key.pem

# Create the root certificate -- give it a long expiration date, if this certificate expires, every certificate that was signed by this one will also expire
# Warning: Whenever you use the `req` tool, you must specify a configuration file to use with the `-config` option, otherwise OpenSSL will default to `/etc/pki/tls/openssl.cnf`.

cd /root/ca
openssl req -config openssl.cnf -key private/ca.key.pem -new -x509 -days 7300 -sha256 -extensions v3_ca -out certs/ca.cert.pem
chmod 444 certs/ca.cert.pem

# verify the root certificate
openssl x509 -noout -text -in certs/ca.cert.pem

# Create the intermediate pair

# An intermediate certificate authority (CA) is an entity that can sign certificates on behalf of the root CA. 
# The root CA signs the intermediate certificate, forming a chain of trust.
# The purpose of using an intermediate CA is primarily for security.
# The root key can be kept offline and used as infrequently as possible.
# If the intermediate key is compromised, the root CA can revoke the intermediate certificate and create a new intermediate cryptographic pair.

# prepare the directory
mkdir /root/ca/intermediate
cd /root/ca/intermediate
mkdir certs crl csr newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial
echo 1000 > /root/ca/intermediate/crlnumber

# modify the downloaded config file
wget https://gitlab.com/martiph/certificate-authority-on-a-raspberry-pi/raw/master/intermediate.openssl.cnf
mv intermediate-config.txt openssl.cnf 
# modify the file!

# create the intermediate key
cd /root/ca
openssl genrsa -aes256 -out intermediate/private/intermediate.key.pem 4096
# enter a strong password

chmod 400 intermediate/private/intermediate.key.pem

# create the intermediate certificate
# warning: Make sure you specify the intermediate CA configuration file (intermediate/openssl.cnf).

cd /root/ca
openssl req -config intermediate/openssl.cnf -new -sha256 -key intermediate/private/intermediate.key.pem -out intermediate/csr/intermediate.csr.pem

# give the following information:
# Country Name: CH
# State or Province Name: Switzerland
# Locality Name: Lyss
# Organization Name: Philip Marti
# Organizational Unit Name: Philip Marti Certification Authority
# Common Name: Philip Marti Root CA
# Email Address: philip_marti@besonet.ch


# warning: This time, specify the root CA configuration file (/root/ca/openssl.cnf).
cd /root/ca
openssl ca -config openssl.cnf -extensions v3_intermediate_ca -days 3650 -notext -md sha256 -in intermediate/csr/intermediate.csr.pem -out intermediate/certs/intermediate.cert.pem

# give the following information:
# Country Name: CH
# State or Province Name: Switzerland
# Locality Name: Lyss
# Organization Name: Philip Marti
# Organizational Unit Name: Philip Marti Certification Authority
# Common Name: Philip Marti Intermediate CA
# Email Address: philip_marti@besonet.ch

chmod 444 intermediate/certs/intermediate.cert.pem
openssl x509 -noout -text -in intermediate/certs/intermediate.cert.pem

# verify the chain of trust
openssl verify -CAfile certs/ca.cert.pem intermediate/certs/intermediate.cert.pem

# create the CA certificate chain
cat intermediate/certs/intermediate.cert.pem certs/ca.cert.pem > intermediate/certs/ca-chain.cert.pem
chmod 444 intermediate/certs/ca-chain.cert.pem

# important note: The steps below are from your perspective as 
# the certificate authority. A third-party, however, can instead 
# create their own private key and certificate signing request (CSR) 
# without revealing their private key to you. They give you their
# CSR, and you give back a signed certificate. In that scenario, 
# skip the genrsa and req commands.

# first create a key
cd /root/ca
openssl genrsa -aes256 -out intermediate/private/www.example.com.key.pem 2048 
chmod 400 intermediate/private/www.example.com.key.pem

# now create a certificate
cd /root/ca
openssl req -config intermediate/openssl.cnf -key intermediate/private/www.example.com.key.pem -new -sha256 -out intermediate/csr/www.example.com.csr.pem

# enter the information for the certificate user like Country code, etc.
# watch here: https://jamielinux.com/docs/openssl-certificate-authority/sign-server-and-client-certificates.html#create-a-certificate which example details were used
cd /root/ca
openssl ca -config intermediate/openssl.cnf -extensions server_cert -days 375 -notext -md sha256 -in intermediate/csr/www.example.com.csr.pem -out intermediate/certs/www.example.com.cert.pem
chmod 444 intermediate/certs/www.example.com.cert.pem

# to verify the certificate
openssl x509 -noout -text -in intermediate/certs/www.example.com.cert.pem

# use the chain file to verify the trust of the certificate
openssl verify -CAfile intermediate/certs/ca-chain.cert.pem intermediate/certs/www.example.com.cert.pem

# to deploy the certificate the following files are used
# * ca-chain.cert.pem
# * www.example.com.key.pem
# * www.example.com.cert.pem
