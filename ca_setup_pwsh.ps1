#!/bin/pwsh

# prepare the directory
Set-Location /
New-Item -ItemType Directory -Path /root/ca
Set-Location /root/ca
New-Item -ItemType Directory -Path certs crl newcerts private
chmod 700 private
New-Item -ItemType File -Path index.txt
Write-Output 1000 > serial

# prepare the configuration file
Invoke-WebRequest https://gitlab.com/martiph/certificate-authority-on-a-raspberry-pi/raw/master/root.openssl.cnf 
mv ./root.openssl.cnf /root/ca/openssl.cnf

# now you need to edit the configuration file

# after you have done the modifications
Set-Location /root/ca
openssl genrsa -aes256 -out private/ca.key.pem 4096

# now add a secure password

chmod 400 private/ca.key.pem

# Create the root certificate -- give it a long expiration date, if this certificate expires, every certificate that was signed by this one will also expire
# Warning: Whenever you use the `req` tool, you must specify a configuration file to use with the `-config` option, otherwise OpenSSL will default to `/etc/pki/tls/openssl.cnf`.

Set-Location /root/ca
openssl req -config openssl.cnf -key private/ca.key.pem -new -x509 -days 7300 -sha256 -extensions v3_ca -out certs/ca.cert.pem
chmod 444 certs/ca.cert.pem

# verify the root certificate
openssl x509 -noout -text -in certs/ca.cert.pem

# Create the intermediate pair

# An intermediate certificate authority (CA) is an entity that can sign certificates on behalf of the root CA. 
# The root CA signs the intermediate certificate, forming a chain of trust.
# The purpose of using an intermediate CA is primarily for security.
# The root key can be kept offline and used as infrequently as possible.
# If the intermediate key is compromised, the root CA can revoke the intermediate certificate and create a new intermediate cryptographic pair.

# prepare the directory
New-Item -ItemType Directory -Path /root/ca/intermediate
Set-Location /root/ca/intermediate
New-Item -ItemType Directory certs crl csr newcerts private
chmod 700 private
New-Item -ItemType File -Path index.txt
Write-Output 1000 > serial
Write-Output 1000 > /root/ca/intermediate/crlnumber

# modify the downloaded config file
Invoke-WebRequest https://gitlab.com/martiph/certificate-authority-on-a-raspberry-pi/raw/master/intermediate.openssl.cnf
Move-Item -Path intermediate-config.txt -Destination openssl.cnf 
# modify the file!

# create the intermediate key
Set-Location /root/ca
openssl genrsa -aes256 -out intermediate/private/intermediate.key.pem 4096
# enter a strong password

chmod 400 intermediate/private/intermediate.key.pem

# create the intermediate certificate
# warning: Make sure you specify the intermediate CA configuration file (intermediate/openssl.cnf).

Set-Location /root/ca
openssl req -config intermediate/openssl.cnf -new -sha256 -key intermediate/private/intermediate.key.pem -out intermediate/csr/intermediate.csr.pem

# give the following information:
# Country Name: CH
# State or Province Name: Switzerland
# Locality Name: Lyss
# Organization Name: Philip Marti
# Organizational Unit Name: Philip Marti Certification Authority
# Common Name: Philip Marti Root CA
# Email Address: philip_marti@besonet.ch


# warning: This time, specify the root CA configuration file (/root/ca/openssl.cnf).
Set-Location /root/ca
openssl ca -config openssl.cnf -extensions v3_intermediate_ca -days 3650 -notext -md sha256 -in intermediate/csr/intermediate.csr.pem -out intermediate/certs/intermediate.cert.pem

# give the following information:
# Country Name: CH
# State or Province Name: Switzerland
# Locality Name: Lyss
# Organization Name: Philip Marti
# Organizational Unit Name: Philip Marti Certification Authority
# Common Name: Philip Marti Intermediate CA
# Email Address: philip_marti@besonet.ch

chmod 444 intermediate/certs/intermediate.cert.pem
openssl x509 -noout -text -in intermediate/certs/intermediate.cert.pem

# verify the chain of trust
openssl verify -CAfile certs/ca.cert.pem intermediate/certs/intermediate.cert.pem

# create the CA certificate chain
Get-Content -Path intermediate/certs/intermediate.cert.pem certs/ca.cert.pem > intermediate/certs/ca-chain.cert.pem
chmod 444 intermediate/certs/ca-chain.cert.pem

# important note: The steps below are from your perspective as 
# the certificate authority. A third-party, however, can instead 
# create their own private key and certificate signing request (CSR) 
# without revealing their private key to you. They give you their
# CSR, and you give back a signed certificate. In that scenario, 
# skip the genrsa and req commands.

# first create a key
Set-Location /root/ca
openssl genrsa -aes256 -out intermediate/private/www.example.com.key.pem 2048 
chmod 400 intermediate/private/www.example.com.key.pem

# now create a certificate
Set-Location /root/ca
openssl req -config intermediate/openssl.cnf -key intermediate/private/www.example.com.key.pem -new -sha256 -out intermediate/csr/www.example.com.csr.pem

# enter the information for the certificate user like Country code, etc.
# watch here: https://jamielinux.com/docs/openssl-certificate-authority/sign-server-and-client-certificates.html#create-a-certificate which example details were used
Set-Location /root/ca
openssl ca -config intermediate/openssl.cnf -extensions server_cert -days 375 -notext -md sha256 -in intermediate/csr/www.example.com.csr.pem -out intermediate/certs/www.example.com.cert.pem
chmod 444 intermediate/certs/www.example.com.cert.pem

# to verify the certificate
openssl x509 -noout -text -in intermediate/certs/www.example.com.cert.pem

# use the chain file to verify the trust of the certificate
openssl verify -CAfile intermediate/certs/ca-chain.cert.pem intermediate/certs/www.example.com.cert.pem

# to deploy the certificate the following files are used
# * ca-chain.cert.pem
# * www.example.com.key.pem
# * www.example.com.cert.pem
